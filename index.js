const express = require('express');
const app = express();
const MongoConn = require('./conn');

const cors = require("cors")

const mongoose = require('mongoose');

const User = require('./models/User');

const jwt = require('jsonwebtoken');


MongoConn.on('connected', function() {
    console.log('MongoDB connected!');
});

app.use(cors())
app.use(express.json());

app.get('/', (req, res) => {
    res.json([
        {"Service Name": "Auth Service", "Service URL": "http://localhost:7070"},
        {"Status"   : "Running"},
    ]);
}
);

// Register a new user

app.post('/api/v1/register',async (req, res) => {

    const {name , email , password} = req.body;

    const UserExists = await User.findOne({email});
    if (UserExists) {
        return res.status(400).json({
            message: 'User already exists'
        });
    }
    else{
        const user = new User({
            name,
            email,
            password
        });
        await user.save();
        res.status(201).json({
            message: 'User created successfully',
            user
        });
    }

    
}); 


// Login a user

app.post('/api/v1/login', async (req, res) => {

    const {email, password} = req.body;

    const user = await User.findOne({email});
    if (!user) {
        return res.status(400).json({
            message: 'User does not exist'
        });
    }
      
    else{

        if (user.password !== password) {
            return res.status(400).json({
                message: 'Incorrect password'
            });
        }
        const payload = {
            email,
            name: user.name
        }
        jwt.sign(payload, 'secret', {expiresIn: '1h'}, (err, token) => {

            if (err) {
                return res.status(500).json({
                    message: 'Error signing token'
                });
            }else{
            res.status(200).json({
                message: 'Login successful',
                user,
                token:token
            });
        }
        }
        );
    }
    
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Auth server listening on port ${PORT}`);
}
);
